<?php

/*
* This file is part of the Fio Bank Account Bundle.
*
* (c) Petr Jaroš <petajaros@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace SimplyNet\FioBankTransferBundle\Client;

class Transaction
{
    /**
     * @var string
     */
    private $id;
    
    /**
     * @var \DateTime
     */
    private $timestamp;
    
    /**
     * @var float 
     */
    private $amount;
    
    /**
     * @var string 
     */
    private $currency;
    
    /**
     * @var Account 
     */
    private $sender;
    
    /**
     * @var Account 
     */
    private $recipient;
    
    /**
     * @var string
     */
    private $constantSymbol;
    
    /**
     * @var string
     */
    private $variableSymbol;
    
    /**
     * @var string
     */
    private $specificSymbol;
    
    /**
     * @var string 
     */
    private $userIdentification;
    
    /**
     * @var string 
     */
    private $senderMessage;
    
    /**
     * @var string 
     */
    private $type;
    
    /**
     * @var string 
     */
    private $payer;
    
    /**
     * @var string 
     */
    private $additionalInformation;
    
    /**
     * @var string 
     */
    private $note;
    
    /**
     * @var string 
     */
    private $orderId;
    
    /**
     * 
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    
    /**
     * 
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * 
     * @param \DateTime $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }
    
    /**
     * 
     * @return \Datetime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }
    
    /**
     * 
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }
    
    /**
     * 
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }
    
    /**
     * 
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }
    
    /**
     * 
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    
    /**
     * 
     * @param Account $sender
     */
    public function setSender(Account $sender)
    {
        $this->sender = $sender;
    }
    
    /**
     * 
     * @return Account
     */
    public function getSender()
    {
        return $this->sender;
    }
    
    /**
     * 
     * @param Account $recipient
     */
    public function setRecipient(Account $recipient)
    {
        $this->recipient = $recipient;
    }
    
    /**
     * 
     * @return Account
     */
    public function getRecipient()
    {
        return $this->recipient;
    }
    
    /**
     * 
     * @param string $constantSymbol
     */
    public function setConstantSymbol($constantSymbol)
    {
        $this->constantSymbol = $constantSymbol;
    }
    
    /**
     * 
     * @return string
     */
    public function getConstantSymbol()
    {
        return $this->constantSymbol;
    }
    
    /**
     * 
     * @param string $variableSymbol
     */
    public function setVariableSymbol($variableSymbol)
    {
        $this->variableSymbol = $variableSymbol;
    }
    
    /**
     * 
     * @return string
     */
    public function getVariableSymbol()
    {
        return $this->variableSymbol;
    }
    
    /**
     * 
     * @param string $specificSymbol
     */
    public function setSpecificSymbol($specificSymbol)
    {
        $this->specificSymbol = $specificSymbol;
    }
    
    /**
     * 
     * @return string
     */
    public function getSpecificSymbol()
    {
        return $this->specificSymbol;
    }
    
    /**
     * 
     * @param string $userIdentification
     */
    public function setUserIdentification($userIdentification)
    {
        $this->userIdentification = $userIdentification;
    }
    
    /**
     * 
     * @return string
     */
    public function getUserIdentification()
    {
        return $this->userIdentification;
    }
    
    /**
     * 
     * @param string $senderMessage
     */
    public function setSenderMessage($senderMessage)
    {
        $this->senderMessage = $senderMessage;
    }
    
    /**
     * 
     * @return string
     */
    public function getSenderMessage()
    {
        return $this->senderMessage;
    }
    
    /**
     * 
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
    
    /**
     * 
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * 
     * @param string $payer
     */
    public function setPayer($payer)
    {
        $this->payer = $payer;
    }
    
    /**
     * 
     * @return string
     */
    public function getPayer()
    {
        return $this->payer;
    }
    
    /**
     * 
     * @param string $additionalInformation
     */
    public function setAdditionalInformation($additionalInformation)
    {
        $this->additionalInformation = $additionalInformation;
    }
    
    /**
     * 
     * @return string
     */
    public function getAdditionalInformation()
    {
        return $this->additionalInformation;
    }
    
    /**
     * 
     * @param string $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }
    
    /**
     * 
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }
    
    /**
     * 
     * @param string $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }
    
    /**
     * 
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     *
     * @return Account
     */
    public function getContraAccount()
    {
        if($this->getAmount() > 0) {
            return $this->sender;
        } else {
            return $this->recipient;
        }
    }
}
