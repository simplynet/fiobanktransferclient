<?php

/*
* This file is part of the Fio Bank Account Bundle.
*
* (c) Petr Jaroš <petajaros@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace SimplyNet\FioBankTransferBundle\Client;

class XmlClient extends Client
{
    /**
     *
     * @param string $token
     * @param string $rootCertificate
     */
    function __construct($token, $rootCertificate = null)
    {
        parent::__construct('xml', $token, $rootCertificate);
    }

    /**
     *
     * @param \DateTime $fromDate
     * @param \DateTime $toDate
     * @return array
     */
    public function getTransactionsByDate(\DateTime $fromDate, \DateTime $toDate)
    {
        $response = $this->getRequestTransactionsByDate($fromDate, $toDate);
        return $this->parseTransactionsData($response);
    }

    /**
     *
     * @param int $year
     * @param int $orderNumber
     * @return array
     */
    public function getTransactionsByOrderNumber($year, $orderNumber) {
        $response = $this->getRequestTransactionsByOrderNumber($year, $orderNumber);
        return $this->parseTransactionsData($response);
    }

    /**
     *
     * @return array
     */
    public function getLastTransactions() {
        $response = $this->getRequestLastTransactions();
        return $this->parseTransactionsData($response);
    }

    /**
     *
     * @param string $data
     * @return \SimplyNet\FioBankTransferBundle\Client\Transaction
     */
    private function parseTransactionsData($data)
    {
        if (!extension_loaded('simplexml')) {
            throw new \RuntimeException('The simpleXML extension must be loaded.');
        }

        $transactions = array();
        $xmlObject = new \SimpleXMLElement($data);

        $recipient = new Account();
        $xmlInfo = $xmlObject->xpath('/AccountStatement/Info')[0];
        if($xmlInfo->accountId) {
            $recipient->setNumber((string)$xmlInfo->accountId);
        }
        if($xmlInfo->iban) {
            $recipient->setIban((string)$xmlInfo->iban);
        }
        if($xmlInfo->bankId) {
            $recipient->setBankId((string)$xmlInfo->bankId);
        }
        if($xmlInfo->bic) {
            $recipient->setBic((string)$xmlInfo->bic);
        }
        $recipient->setBankName('Fio banka, a.s.');


        $xmlTransactions = $xmlObject->xpath('/AccountStatement/TransactionList/Transaction');
        foreach($xmlTransactions as $xmlTransaction)
        {
            $transaction = new Transaction();
            if($xmlTransaction->column_22) {
                $transaction->setId((string)$xmlTransaction->column_22);
            }
            if($xmlTransaction->column_0) {
                $transaction->setTimestamp(new \DateTime((string)$xmlTransaction->column_0));
            }
            if($xmlTransaction->column_1) {
                $transaction->setAmount((float)$xmlTransaction->column_1);
            }
            if($xmlTransaction->column_14) {
                $transaction->setCurrency((string)$xmlTransaction->column_14);
            }
            if($xmlTransaction->column_4) {
                $transaction->setConstantSymbol($xmlTransaction->column_4);
            }
            if($xmlTransaction->column_5) {
                $transaction->setVariableSymbol($xmlTransaction->column_5);
            }
            if($xmlTransaction->column_6) {
                $transaction->setSpecificSymbol($xmlTransaction->column_6);
            }
            if($xmlTransaction->column_7) {
                $transaction->setUserIdentification((string)$xmlTransaction->column_7);
            }
            if($xmlTransaction->column_16) {
                $transaction->setSenderMessage((string)$xmlTransaction->column_16);
            }
            if($xmlTransaction->column_8) {
                $transaction->setType((string)$xmlTransaction->column_8);
            }
            if($xmlTransaction->column_9) {
                $transaction->setPayer((string)$xmlTransaction->column_9);
            }
            if($xmlTransaction->column_18) {
                $transaction->setAdditionalInformation((string)$xmlTransaction->column_18);
            }
            if($xmlTransaction->column_25) {
                $transaction->setNote((string)$xmlTransaction->column_25);
            }
            if($xmlTransaction->column_17) {
                $transaction->setOrderId((string)$xmlTransaction->column_17);
            }

            $sender = new Account();
            if($xmlTransaction->column_2) {
                $sender->setNumber((string)$xmlTransaction->column_2);
            }
            if($xmlTransaction->column_10) {
                $sender->setName((string)$xmlTransaction->column_10);
            }
            if($xmlTransaction->column_3) {
                $sender->setBankId((string)$xmlTransaction->column_3);
            }
            if($xmlTransaction->column_12) {
                $sender->setBankName((string)$xmlTransaction->column_12);
            }
            if($xmlTransaction->column_26) {
                $sender->setBic((string)$xmlTransaction->column_26);
            }

            if($transaction->getAmount() > 0) {
                if(!$sender->isEmpty()) $transaction->setSender($sender);
                if(!$recipient->isEmpty()) $transaction->setRecipient($recipient);
            } else {
                if(!$recipient->isEmpty()) $transaction->setSender($recipient);
                if(!$sender->isEmpty()) $transaction->setRecipient($sender);
            }

            $transactions[] = $transaction;
        }
        return $transactions;
    }
}
