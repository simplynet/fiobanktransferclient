<?php

/*
* This file is part of the Fio Bank Account Bundle.
*
* (c) Petr Jaroš <petajaros@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace SimplyNet\FioBankTransferBundle\Client;

class Account
{
    /**
     * @var string
     */
    private $number;
    
    /**
     * @var string
     */
    private $bankId;
    
    /**
     * @var string
     */
    private $bankName;
    
    /**
     * @var string 
     */
    private $iban;
    
    /**
     * @var string 
     */
    private $bic;
    
    /**
     * @var string
     */
    private $name;
    
    /**
     * 
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }
    
    /**
     * 
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }
    
    /**
     * 
     * @param string $bankId
     */
    public function setBankId($bankId)
    {
        $this->bankId = $bankId;
    }
    
    /**
     * 
     * @return string
     */
    public function getBankId()
    {
        return $this->bankId;
    }
    
    /**
     * 
     * @param string $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }
    
    /**
     * 
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }
    
    /**
     * 
     * @param string $iban
     */
    public function setIban($iban)
    {
        $this->iban = $iban;
    }
    
    /**
     * 
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }
    
    /**
     * 
     * @param string $bic
     */
    public function setBic($bic)
    {
        $this->bic = $bic;
    }
    
    /**
     * 
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }
    
    /**
     * 
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     * @return boolean
     */
    public function isEmpty()
    {
        if($this->number === null &&
            $this->bankId === null &&
            $this->bankName === null &&
            $this->iban === null &&
            $this->bic === null &&
            $this->name === null) {
            return true;
        }
        return false;
    }
}
